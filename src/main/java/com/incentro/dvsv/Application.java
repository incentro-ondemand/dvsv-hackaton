package com.incentro.dvsv;

import org.camunda.bpm.application.ProcessApplication;
import org.camunda.bpm.application.impl.ServletProcessApplication;

@ProcessApplication("DVSV")
public class Application  extends ServletProcessApplication {
    // empty implementation
}
