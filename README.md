# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary



## Quick summary ##
This project is a basic Camunda Java project for the DVSV process.
Make sure there is already a process named DVSV moddeled with the modeler and saved to this projects 
'src/main/resource' folder.

For this project I used the Tomcat distribution of Camunda, also see the following guide:
[Get started with Camunda and BPMN 2.0](https://docs.camunda.org/get-started/java-process-app/)
